package com.captton.programa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String resultado = "";
		
		float numerador, denominador;
		
		Scanner scanner = new Scanner(System.in);
		
		try {
		
		System.out.println("Ingrese el numerador: ");
		numerador = scanner.nextInt();
		
		System.out.println("Ingrese el denominador");
		denominador = scanner.nextInt();
		
		resultado = String.valueOf(numerador / denominador);
		
		System.out.println("Resultado es:"+resultado);
		
		}catch (ArithmeticException e) {
			
			System.out.println("No se puede dividir por cero");
		}catch (InputMismatchException e) {
			
			System.out.println("No se pueden operar letras");
		}
		
		scanner.close();
		
		
		
		
	}

}
